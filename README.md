# trello.com
## Links
* [https://trello.com/](https://trello.com/)
* [API Introduction](https://developers.trello.com/docs/api-introduction)
* [API BOARDS](https://developers.trello.com/reference#boards-2)
* [API LISTS](https://developers.trello.com/reference#lists)
* [API CARDS](https://developers.trello.com/reference#cards-1)

## Examples
```bash
# Creds
trelloKey="$(cat trello-key.txt)";\
trelloToken="$(cat trello-token.txt)";

# Get boards
curl "https://api.trello.com/1/members/me/boards?fields=name,url&key=${trelloKey}&token=${trelloToken}"

# Get lists in board
boardId='';\
curl "https://api.trello.com/1/boards/${boardId}/lists?key=${trelloKey}&token=${trelloToken}"


# Get cards in list
listId='';\
curl "https://api.trello.com/1/lists/${listId}/cards?fields=id,name&key=${trelloKey}&token=${trelloToken}"

# Get card by id
cardId='';\
curl "https://api.trello.com/1/cards/${cardId}?key=${trelloKey}&token=${trelloToken}"
# or
curl "https://api.trello.com/1/cards/${cardId}?fields=id,name,desc&key=${trelloKey}&token=${trelloToken}"
curl "https://api.trello.com/1/cards/${cardId}?fields=all&key=${trelloKey}&token=${trelloToken}"

# Put card in list
listId='';\
myCardName='new';\
myCardDesc='http%3A%2F%2Fya.ru';\
curl --request POST \
--url "https://api.trello.com/1/cards?name=${myCardName}&desc=${myCardDesc}&idList=${listId}&keepFromSource=all&key=${trelloKey}&token=${trelloToken}"

```
